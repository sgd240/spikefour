// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "SPFourPlayerPawn.generated.h"

UCLASS()
class SPIKEFOUR_API ASPFourPlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASPFourPlayerPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	UPROPERTY(EditAnywhere)
		USceneComponent* OurVisibleComponent;
	UFUNCTION(BlueprintCallable, Category = "WeLikeToMove")
		void Move_XAxis(float AxisValue);
	UFUNCTION(BlueprintCallable, Category = "WeLikeToMove")
		void Move_YAxis(float AxisValue);
	FVector CurrentVelocity;

	
	
};
