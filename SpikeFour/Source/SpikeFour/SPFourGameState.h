// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameStateBase.h"
#include "SPFourGameState.generated.h"

/**
 * 
 */
UCLASS()
class SPIKEFOUR_API ASPFourGameState : public AGameStateBase
{
	GENERATED_BODY()
		ASPFourGameState();
	
public: 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Scoredat")
	int score;
	UFUNCTION(BlueprintCallable, Category = "Scoredat")
		void AddScore();
	
	
	
};
